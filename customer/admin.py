#coding: utf-8

from django.contrib import admin
from models import Customer

# Register your models here.
class CustomerAdmin(admin.ModelAdmin):
	def __unicode__(self):
		return u'%s' % self.pk

	class Meta:
		model = Customer
		verbose_name = u'Заказчик'
		verbose_name_plural = u'Заказчики'

		fieldsets = [
		    (u'Ответственное лицо', {'fields': ['fam' ,'name', 'otch', 'phone']}),
		    (u'Организация', {'fields': ['fullname', 'shortname', 'fam_ruk', 'name_ruk', 'otch_ruk']}),
		]

admin.site.register(Customer, CustomerAdmin)
