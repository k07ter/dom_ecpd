from django.conf.urls import patterns, include, url
from django.contrib import admin
from . import views #import home, cust, inv

urlpatterns = patterns('',
    # Examples:
    url(r'^$', views.home, name='home'),
    url(r'^inv/$', views.inv, name='inv'),
    url(r'^inv/(\d+)/$', views.inv, name='inv'),
    #url(r'^spr/(\d+)/$', views.spr, name='spr'),
    url(r'^(\d+)/$', views.cust, name='cust'),
)
