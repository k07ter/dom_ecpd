#coding: utf-8

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.shortcuts import render, render_to_response

from ecpd.models import Person, Sprorg, Prox
PAYMENT_PERIOD = (
	('W', u'Неделя'),
	('M', u'Месяц'),
)
PAYMENT_STATUS = (
	(1, u'Оплачено'),
	(0, u'Не оплачено'),
)

# Create your models here.
class Customer(Person):
	#custid = models.CharField(u'ID заказчика', max_length=12)
	org = models.ForeignKey(Sprorg, verbose_name=u'Организация')
	fam_ruk = models.CharField(u'Фамилия', max_length=25)
	name_ruk = models.CharField(u'Имя', max_length=15)
	otch_ruk = models.CharField(u'Отчество', max_length=35)
	#fam_usr = models.CharField(u'Фамилия:', max_length=25)
	#name_usr = models.CharField(u'Имя:', max_length=15)
	#otch_usr = models.CharField(u'Отчество:', max_length=35)
	status_usr = models.CharField(u'Должность', max_length=35)
	#phone_usr = models.CharField(u'Телефон основного пользователя:', max_length=10)
	#email_usr = models.EmailField(u'Электронная почта основного пользователя:')
	#other_cont_usr = models.CharField(u'Другие контакты:', max_length=100, blank=True)
	#login = models.CharField(u'Логин:', max_length=20)

	#payment_period_type = models.CharField(u'Период оплаты', choices=PAYMENT_PERIOD, default='w', max_length=3)
	#payment_status = models.IntegerField(u'Статус оплаты', choices=PAYMENT_STATUS, default=0)
	#documenty = models.FileField(u'Юридические документы')

	def show_demo(self):
		cust = [] #Customer.objects.using('cust').all()
		return render_to_response('customer/index.html', {'cust': cust})

	class Meta:
		db_table = 'customers'
		verbose_name = _(u'Заказчик')
		verbose_name_plural = _(u'Заказчики')

	def __unicode__(self):
		return u'%s, %s' % (self.fam, self.name)
