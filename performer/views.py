#coding: utf-8

from django.shortcuts import render, render_to_response
from models import Performer

# Create your views here.
def home(request):
	all_perf = [{'name': u'Мила'}, {'name': u'Лена'}]
	#perf_db = Performer.objects.using('performer').all()
	#print cust_db
	return render_to_response('performer/index.html', {'all_cust': all_perf}) #, 'index.html')

def perf(request, arg):
	print(arg)
	#all_perf = [{'name': u'Аня', 'nom': 2}, {'name': u'Маня', 'nom':3}]
	all_perf = Performer.objects.using('perf').filter(nom=arg)
	return render_to_response('%s/index.html' % 'performer', {'all_cust': all_perf})

