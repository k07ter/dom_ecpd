# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Performer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fam', models.CharField(max_length=20, verbose_name='\u0424\u0430\u043c\u0438\u043b\u0438\u044f')),
                ('name', models.CharField(max_length=15, verbose_name='\u0418\u043c\u044f')),
                ('otch', models.CharField(max_length=30, verbose_name='\u041e\u0442\u0447\u0435\u0441\u0442\u0432\u043e')),
                ('phone', models.CharField(max_length=10, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d')),
                ('email', models.EmailField(unique=True, max_length=255)),
                ('att_nom', models.CharField(max_length=10, verbose_name='\u041d\u043e\u043c\u0435\u0440 \u0430\u0442\u0442\u0435\u0441\u0442\u0430\u0442\u0430')),
                ('news_signed', models.BooleanField(default=False, verbose_name='\u041f\u043e\u0434\u043f\u0438\u0441\u043a\u0430 \u043d\u0430 \u043d\u043e\u0432\u043e\u0441\u0442\u0438')),
                ('kod', models.CharField(max_length=10, verbose_name='\u041a\u043e\u0434')),
                ('documenty', models.FileField(upload_to=b'/upload/docs/', verbose_name='\u0414\u043e\u043a\u0443\u043c\u0435\u043d\u0442\u044b', blank=True)),
            ],
            options={
                'db_table': 'performers',
                'verbose_name': '\u041f\u043e\u0434\u0440\u044f\u0434\u0447\u0438\u043a',
                'verbose_name_plural': '\u041f\u043e\u0434\u0440\u044f\u0434\u0447\u0438\u043a\u0438',
            },
            bases=(models.Model,),
        ),
    ]
