#coding: utf-8

from django.db import models
from ecpd.models import Person

from ecpd import settings

# Create your models here.
class Performer(Person):
	#email = models.EmailField(u'Эл. ящик')
	kod = models.CharField(u'Код', max_length=10)
	#phone = models.CharField(u'Телефон', max_length=12)
	#work_kind = models.ForeignKey(u'Тип деятельности', 'Jobkind')
	#news_signed = models.BooleanField(u'Подписка на новости', blank=True, default=False)
	documenty = models.FileField(u'Документы', upload_to=settings.UPLOADS_ROOT+'docs/', blank=True)

	class Meta:
		db_table = 'performers'
		verbose_name = u'Подрядчик'
		verbose_name_plural = u'Подрядчики'

	def __unicode__(self):
		return u'%s, %s' % (self.fam, self.name)

