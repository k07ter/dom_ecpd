from django.conf.urls import patterns, include, url
from django.contrib import admin
from . import views

urlpatterns = patterns('',
    # Examples:
    url(r'/$', views.home, name='perf'),
    url(r'/(\d+)', views.perf, name='perf'),
)
