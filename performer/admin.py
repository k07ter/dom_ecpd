#coding: utf-8

from django.contrib import admin
from models import Performer

# Register your models here.
class PerformerAdmin(admin.ModelAdmin):
	#def __unicode__(self):
	#	return u'%s' % self.pk
	class Meta:
		model = Performer
		db_name = 'performers'

	verbose_name = u'Подрядчик'
	verbose_name_plural = u'Подрядчики'

admin.site.register(Performer, PerformerAdmin)

