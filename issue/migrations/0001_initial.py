# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('performer', '__first__'),
        ('ecpd', '0001_initial'),
        ('customer', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Issue',
            fields=[
                ('message_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='ecpd.Message')),
                ('issue_nom', models.CharField(max_length=10, verbose_name='\u041d\u043e\u043c\u0435\u0440 \u0437\u0430\u044f\u0432\u043a\u0438')),
                ('status', models.IntegerField(blank=True, verbose_name='\u0420\u0435\u0448\u0435\u043d\u0438\u0435 \u043f\u043e \u041c\u041f', choices=[(1, '\u041e\u0434\u043e\u0431\u0440\u0435\u043d\u043e'), (0, '\u041e\u0442\u043a\u0430\u0437\u0430\u043d\u043e')])),
                ('date', models.DateTimeField(default=datetime.datetime.now, verbose_name='\u0414\u0430\u0442\u0430 \u0440\u0435\u0448\u0435\u043d\u0438\u044f')),
                ('cust', models.ForeignKey(verbose_name='\u0417\u0430\u043a\u0430\u0437\u0447\u0438\u043a', to='customer.Customer')),
                ('perf', models.ForeignKey(verbose_name='\u0418\u0441\u043f\u043e\u043b\u043d\u0438\u0442\u0435\u043b\u044c', to='performer.Performer')),
            ],
            options={
                'db_table': 'issues',
                'verbose_name': '\u0417\u0430\u044f\u0432\u043a\u0430/\u0421\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u0435',
                'verbose_name_plural': '\u0417\u0430\u044f\u0432\u043a\u0438/\u0421\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u044f',
            },
            bases=('ecpd.message',),
        ),
    ]
