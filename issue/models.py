#coding: utf-8

from django.db import models
from customer.models import *
from performer.models import *
from ecpd.models import Message
from datetime import datetime

ISSUE_STAT = (
	(1, u'Одобрено'),
	(0, u'Отказано'),
)

# Create your models here.
class Issue(Message):
	issue_nom = models.CharField(u'Номер заявки', max_length=10)
	cust = models.ForeignKey(Customer, verbose_name=u'Заказчик')
        perf = models.ForeignKey(Performer, verbose_name=u'Исполнитель')
	#kurat = models.ForeignKey(Performer, verbose_name=u'Куратор')
	status = models.IntegerField(u'Решение по МП', choices=ISSUE_STAT, blank=True)
	date = models.DateTimeField(u'Дата решения', default=datetime.now)

	#def list(self, request):
	#	return self.objects

	class Meta:
		db_table = 'issues'
		verbose_name = u'Заявка/Сообщение'
		verbose_name_plural = u'Заявки/Сообщения'

	def __unicode__(self):
		return u'%s' % self.issue_nom

#class Service():
#	name = models.CharField(u'Наименование', max_length=10)
