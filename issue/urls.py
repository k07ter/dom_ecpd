from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    url(r'/$', 'issue.views.home', name='home'),
    url(r'/(\d+)', 'issue.views.iss', name='cust'),
    url(r'/add$', 'issue.views.new_issue', name='home'),
)
