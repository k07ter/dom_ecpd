#coding: utf-8

from django.shortcuts import render, render_to_response
from models import Issue

# Create your views here.
def home(request):
	#all_cust = [{'name': u'Миша'}, {'name': u'Петя'}]
	iss_db = Issue.objects.all()
	print iss_db
	return render_to_response('issue/index.html', {'issues': iss_db})

def iss(request, arg=None):
	print(arg)
	#all_iss = [{'name': u'Аня', 'nom': 2}, {'name': u'Маня', 'nom':3}]
	iss_one = [Issue.objects.get(issue_nom=arg)]
	#print iss_one.cust_id
	return render_to_response('issue/issue.html', {'issues': iss_one})

def new_issue(request):
	iss_db = Issue.objects.all()
	#all_iss = [{'name': u'Аня', 'nom': 2}, {'name': u'Маня', 'nom':3}][0]
	return render_to_response('issue/issue.html', {'iss': iss_db})

