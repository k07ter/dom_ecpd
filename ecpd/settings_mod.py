#coding: utf-8

RESOURCE_METHODS = ['GET', 'POST', 'DELETE']
ITEM_METHODS = ['GET', 'PATCH', 'PUT', 'DELETE']

DOMAIN = {
    # ��������� ������ `/users`
    'users': {
        # ����� �� ��������� ������ ������. ��� ��������� ������������ ������ Cerberus �� ������ Eve.
        # �� ������ ������������ � ��� � ����������� ������������ ������ http://docs.python-cerberus.org/en/stable/.
        # ���� ��������� ������� � ����������� ������������ EVE http://python-eve.org/validation.html#validation.
        'schema': {
            'username': {
                'type': 'string',
                'minlength': 5,
                'maxlength': 32,
                'required': True,
                # ���������� ���� (������ �� ��������, ������ �������� ������ ���� ����������)
                'unique': True,
            },
            'firstname': {
                'type': 'string',
                'minlength': 1,
                'maxlength': 10,
                'required': True,
            },
            'lastname': {
                'type': 'string',
                'minlength': 1,
                'maxlength': 15,
                'required': True,
            },
            'role': {
                
                'type': 'list', # ���: ������
                'allowed': ["author", "contributor"], # ��������� ������������ ��������: "author", "contributor"
            },
            'location': {
                'type': 'dict', # ���: �������
                # ��������� "�����" �������
                'schema': {
                    'address': {'type': 'string'},
                    'city': {'type': 'string'}
                },
            },
            'born': {
                'type': 'datetime',
            },
            'active': {
                'type': 'boolean',
                'default': True
            }
        }
    },

    # ��������� ������ `/groups`
    'groups': {
        # ��������� ������ ������ (��. ����).
        'schema': {
            'title': {
                'type': 'string',
                'minlength': 5,
                'maxlength': 32,
                'required': True,
                'unique': True
            },
            'users': {
                'type': 'list',  # ���: ������
                'default': [],   # �� ���������: ������ ������
                # ��������� "�����" ������
                'schema': { 
                    'type': 'objectid', # ��� ������: objectid
                    # ��������� �� ������ � ������ ���������
                    'data_relation': {
                        'resource': 'users',  # �� ������ `users` (������� �� ������� ����)
                        'field': '_id',  # �� ���� `_id`
                        'embeddable': True
                    }
                }
            }
        }
    }
}

STATIC_ROOT = ''
MEDIA_URL = ''
