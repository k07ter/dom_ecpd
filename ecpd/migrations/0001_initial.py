# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fam', models.CharField(max_length=20, verbose_name='\u0424\u0430\u043c\u0438\u043b\u0438\u044f')),
                ('name', models.CharField(max_length=15, verbose_name='\u0418\u043c\u044f')),
                ('otch', models.CharField(max_length=30, verbose_name='\u041e\u0442\u0447\u0435\u0441\u0442\u0432\u043e')),
                ('phone', models.CharField(max_length=10, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d')),
                ('email', models.EmailField(unique=True, max_length=255)),
                ('att_nom', models.CharField(max_length=10, verbose_name='\u041d\u043e\u043c\u0435\u0440 \u0430\u0442\u0442\u0435\u0441\u0442\u0430\u0442\u0430')),
                ('news_signed', models.BooleanField(default=False, verbose_name='\u041f\u043e\u0434\u043f\u0438\u0441\u043a\u0430 \u043d\u0430 \u043d\u043e\u0432\u043e\u0441\u0442\u0438')),
                ('fam_ruk', models.CharField(max_length=25, verbose_name='\u0424\u0430\u043c\u0438\u043b\u0438\u044f')),
                ('name_ruk', models.CharField(max_length=15, verbose_name='\u0418\u043c\u044f')),
                ('otch_ruk', models.CharField(max_length=35, verbose_name='\u041e\u0442\u0447\u0435\u0441\u0442\u0432\u043e')),
                ('status_usr', models.CharField(max_length=35, verbose_name='\u0414\u043e\u043b\u0436\u043d\u043e\u0441\u0442\u044c')),
            ],
            options={
                'db_table': 'customers',
                'verbose_name': '\u0417\u0430\u043a\u0430\u0437\u0447\u0438\u043a',
                'verbose_name_plural': '\u0417\u0430\u043a\u0430\u0437\u0447\u0438\u043a\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('anons', models.CharField(max_length=80, verbose_name='\u0422\u0435\u043c\u0430')),
                ('content', models.CharField(max_length=255, verbose_name='\u0421\u043e\u0434\u0435\u0440\u0436\u0430\u043d\u0438\u0435')),
                ('sended', models.BooleanField(default=False, verbose_name='\u041e\u0442\u043f\u0440\u0430\u0432\u043b\u0435\u043d\u043e')),
                ('response', models.BooleanField(default=False, verbose_name='\u041e\u0442\u0432\u0435\u0442')),
            ],
            options={
                'db_table': 'messages',
                'verbose_name': '\u0421\u043e\u043e\u0431\u0448\u0435\u043d\u0438\u0435',
                'verbose_name_plural': '\u0421\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u044f',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Issue',
            fields=[
                ('message_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='ecpd.Message')),
                ('issue_nom', models.CharField(max_length=10, verbose_name='\u041d\u043e\u043c\u0435\u0440 \u0437\u0430\u044f\u0432\u043a\u0438')),
                ('status', models.IntegerField(blank=True, verbose_name='\u0420\u0435\u0448\u0435\u043d\u0438\u0435 \u043f\u043e \u041c\u041f', choices=[(1, '\u041e\u0434\u043e\u0431\u0440\u0435\u043d\u043e'), (0, '\u041e\u0442\u043a\u0430\u0437\u0430\u043d\u043e')])),
                ('date', models.DateTimeField(default=datetime.datetime.now, verbose_name='\u0414\u0430\u0442\u0430 \u0440\u0435\u0448\u0435\u043d\u0438\u044f')),
                ('cust', models.ForeignKey(verbose_name='\u0417\u0430\u043a\u0430\u0437\u0447\u0438\u043a', to='ecpd.Customer')),
            ],
            options={
                'db_table': 'issues',
                'verbose_name': '\u0417\u0430\u044f\u0432\u043a\u0430/\u0421\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u0435',
                'verbose_name_plural': '\u0417\u0430\u044f\u0432\u043a\u0438/\u0421\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u044f',
            },
            bases=('ecpd.message',),
        ),
        migrations.CreateModel(
            name='Messagekind',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('kind', models.CharField(max_length=30, verbose_name='\u0422\u0438\u043f \u0441\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u044f')),
            ],
            options={
                'db_table': 'messagekinds',
                'verbose_name': '\u0422\u0438\u043f \u0441\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u044f',
                'verbose_name_plural': '\u0422\u0438\u043f\u044b \u0441\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u0439',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Performer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fam', models.CharField(max_length=20, verbose_name='\u0424\u0430\u043c\u0438\u043b\u0438\u044f')),
                ('name', models.CharField(max_length=15, verbose_name='\u0418\u043c\u044f')),
                ('otch', models.CharField(max_length=30, verbose_name='\u041e\u0442\u0447\u0435\u0441\u0442\u0432\u043e')),
                ('phone', models.CharField(max_length=10, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d')),
                ('email', models.EmailField(unique=True, max_length=255)),
                ('att_nom', models.CharField(max_length=10, verbose_name='\u041d\u043e\u043c\u0435\u0440 \u0430\u0442\u0442\u0435\u0441\u0442\u0430\u0442\u0430')),
                ('news_signed', models.BooleanField(default=False, verbose_name='\u041f\u043e\u0434\u043f\u0438\u0441\u043a\u0430 \u043d\u0430 \u043d\u043e\u0432\u043e\u0441\u0442\u0438')),
                ('documenty', models.FileField(upload_to=b'/upload/docs/', verbose_name='\u0414\u043e\u043a\u0443\u043c\u0435\u043d\u0442\u044b', blank=True)),
            ],
            options={
                'db_table': 'performers',
                'verbose_name': '\u041f\u043e\u0434\u0440\u044f\u0434\u0447\u0438\u043a',
                'verbose_name_plural': '\u041f\u043e\u0434\u0440\u044f\u0434\u0447\u0438\u043a\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Sitemap',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('title', models.CharField(max_length=30, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a')),
                ('url', models.CharField(max_length=30, verbose_name='\u0421\u0441\u044b\u043b\u043a\u0430')),
                ('path', models.CharField(max_length=255, verbose_name='\u041f\u0443\u0442\u044c \u043d\u0430 \u0434\u0438\u0441\u043a\u0435', blank=True)),
                ('block', models.CharField(blank=True, max_length=5, verbose_name='\u0411\u043b\u043e\u043a \u0432 \u0448\u0430\u0431\u043b\u043e\u043d\u0435', choices=[(b'h', '\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a'), (b'tm', '\u041c\u0435\u043d\u044e \u0441\u0430\u0439\u0442\u0430'), (b'tm2', '\u041f\u043e\u0434 \u043c\u0435\u043d\u044e'), (b'lm', '\u041c\u0435\u043d\u044e \u0443\u0441\u043b\u0443\u0433'), (b'r', '\u0420\u0435\u043a\u043b\u0430\u043c\u0430'), (b'g', '\u0413\u0430\u043b\u0435\u0440\u0435\u044f')])),
                ('accessmode', models.IntegerField(default=0, verbose_name='\u0422\u0438\u043f \u0434\u043e\u0441\u0442\u0443\u043f\u0430', choices=[(0, '\u0421\u0432\u043e\u0431\u043e\u0434\u043d\u044b\u0439'), (1, '\u041f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044c'), (2, '\u0410\u0434\u043c\u0438\u043d')])),
            ],
            options={
                'db_table': 'sitemap',
                'verbose_name': '\u0421\u0442\u0440\u0430\u043d\u0438\u0446\u0430',
                'verbose_name_plural': '\u0421\u0442\u0440\u0430\u043d\u0438\u0446\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Sprkind',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('kod', models.CharField(max_length=10, null=True, verbose_name='\u041a\u043e\u0434', blank=True)),
                ('nazv', models.CharField(max_length=30, verbose_name='\u041d\u0430\u0438\u043c\u0435\u043d\u043e\u0432\u0430\u043d\u0438\u0435')),
            ],
            options={
                'db_table': 'sprkind',
                'verbose_name': '\u0421\u0444\u0435\u0440\u0430 \u0434\u0435\u044f\u0442\u0435\u043b\u044c\u043d\u043e\u0441\u0442\u0438',
                'verbose_name_plural': '\u0421\u0444\u0435\u0440\u044b \u0434\u0435\u044f\u0442\u0435\u043b\u044c\u043d\u043e\u0441\u0442\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Sprorg',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('kod', models.IntegerField(null=True, verbose_name='\u041a\u043e\u0434', blank=True)),
                ('fullname', models.CharField(max_length=255, verbose_name='\u041f\u043e\u043b\u043d\u043e\u0435 \u043d\u0430\u0438\u043c\u0435\u043d\u043e\u0432\u0430\u043d\u0438\u0435')),
                ('shortname', models.CharField(max_length=255, verbose_name='\u041a\u0440\u0430\u0442\u043a\u043e\u0435 \u043d\u0430\u0438\u043c\u0435\u043d\u043e\u0432\u0430\u043d\u0438\u0435')),
                ('adress_ur', models.CharField(max_length=255, verbose_name='\u042e\u0440\u0438\u0434\u0438\u0447\u0435\u0441\u043a\u0438\u0439 \u0430\u0434\u0440\u0435\u0441')),
                ('phone_ur', models.CharField(max_length=10, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d \u043e\u0440\u0433\u0430\u043d\u0438\u0437\u0430\u0446\u0438\u0438:')),
                ('email_ur', models.EmailField(max_length=75, verbose_name='\u042d\u043b\u0435\u043a\u0442\u0440\u043e\u043d\u043d\u0430\u044f \u043f\u043e\u0447\u0442\u0430:')),
                ('ogrn', models.CharField(max_length=15, verbose_name='\u041e\u0413\u0420\u041d')),
                ('inn', models.CharField(max_length=14, verbose_name='\u0418\u041d\u041d')),
                ('date_reg', models.DateField(default=datetime.datetime.now, verbose_name='\u0414\u0430\u0442\u0430 \u0440\u0435\u0433\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u0438 \u043d\u0430 \u043f\u043e\u0440\u0442\u0430\u043b\u0435')),
                ('type_ur', models.ForeignKey(verbose_name='\u0421\u0444\u0435\u0440\u0430 \u0434\u0435\u044f\u0442\u0435\u043b\u044c\u043d\u043e\u0441\u0442\u0438', to='ecpd.Sprkind')),
            ],
            options={
                'db_table': 'sprorg',
                'verbose_name': '\u041e\u0440\u0433\u0430\u043d\u0438\u0437\u0430\u0446\u0438\u044f',
                'verbose_name_plural': '\u041e\u0440\u0433\u0430\u043d\u0438\u0437\u0430\u0446\u0438\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='message',
            name='kat',
            field=models.ForeignKey(verbose_name='\u0422\u0438\u043f \u0441\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u044f', to='ecpd.Messagekind'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='issue',
            name='perf',
            field=models.ForeignKey(verbose_name='\u0418\u0441\u043f\u043e\u043b\u043d\u0438\u0442\u0435\u043b\u044c', to='ecpd.Performer'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='customer',
            name='org',
            field=models.ForeignKey(verbose_name='\u041e\u0440\u0433\u0430\u043d\u0438\u0437\u0430\u0446\u0438\u044f', to='ecpd.Sprorg'),
            preserve_default=True,
        ),
    ]
