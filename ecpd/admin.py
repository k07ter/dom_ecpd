#coding: utf-8

from django.contrib import admin
from ecpd.models import *
#from ecpd.models import Issue, Sitemap

# Register your models here.
class SitemapAdmin(admin.ModelAdmin):
	list_display = ('url', 'title', 'accessmode')
	class Meta:
		model = Sitemap
		#verbose_name = u'Страница'
		#verbose_name_plural = u'Страницы'

class MessagekindAdmin(admin.ModelAdmin):
	class Meta:
		model = Messagekind
		#verbose_name = u'Тип сообщения'
		#verbose_name_plural = u'Типы сообщений'

#class MessageAdmin(admin.ModelAdmin):
#	class Meta:
#		model = Message

class SprorgAdmin(admin.ModelAdmin):
	class Meta:
		model = Sprorg
		verbose_name = u'Организация'

class SprkindAdmin(admin.ModelAdmin):
	class Meta:
		model = Sprkind
		verbose_name = u'Сфера деятельности'

#class IssueAdmin(admin.ModelAdmin):
#	class Meta:
#		model = Issue

#	verbose_name = u'Заявка'
#	verbose_name_plural = u'Заявки'

class PostBaseAdmin(admin.ModelAdmin):
    list_display = ('content', 'author', 'pub_date')
    search_fields = ('content', 'author__username')
    list_filter = ('moderation',)
    actions = ['make_spam', 'make_not_moderated', 'make_post_moderated', 'make_moderated']
 
    def moderate(self, request, rows_updated, choice_description):
        if rows_updated == 1:
            message_bit = "1 запись помечена, как %s" % choice_description
        else:
            message_bit = "%s записей отмечены, как %s." % (rows_updated, choice_description)
        self.message_user(request, "%s" % message_bit)
 
    def make_spam(self, request, queryset):
        self.moderate(
            request=request,
            rows_updated=queryset.update(moderation=PostBase.SPAM),
            choice_description="SPAM"
        )
    make_spam.short_description = "Отметить помеченные, как SPAM"
 
    def make_not_moderated(self, request, queryset):
        self.moderate(
            request=request,
            rows_updated=queryset.update(moderation=PostBase.NOT_MODERATED),
            choice_description="NOT_MODERATED"
        )
    make_not_moderated.short_description = "Отметить помеченные, как NOT_MODERATED"
 
    def make_post_moderated(self, request, queryset):
        self.moderate(
            request=request,
            rows_updated=queryset.update(moderation=PostBase.POST_MODERATED),
            choice_description="POST_MODERATED"
        )
    make_post_moderated.short_description = "Отметить помеченные, как POST_MODERATED"
 
    def make_moderated(self, request, queryset):
        self.moderate(
            request=request,
            rows_updated=queryset.update(moderation=PostBase.MODERATED),
            choice_description="MODERATED"
        )
    make_moderated.short_description = "Отметить помеченные, как MODERATED"

class PerformerAdmin(admin.ModelAdmin):
	#def __unicode__(self):
	#	return u'%s' % self.pk
	class Meta:
		model = Performer
		db_name = 'performers'

	verbose_name = u'Подрядчик'
	verbose_name_plural = u'Подрядчики'

class CustomerAdmin(admin.ModelAdmin):
	def __unicode__(self):
		return u'%s' % self.pk

	class Meta:
		model = Customer
		verbose_name = u'Заказчик'
		verbose_name_plural = u'Заказчики'

		fieldsets = [
		    (u'Ответственное лицо', {'fields': ['fam' ,'name', 'otch', 'phone']}),
		    (u'Организация', {'fields': ['fullname', 'shortname', 'fam_ruk', 'name_ruk', 'otch_ruk']}),
		]

class IssueAdmin(admin.ModelAdmin):
	def __unicode__(self):
		return 'issue'
		
	class Meta:
		model = Issue
		verbose_name = u'Заявка'
		verbose_name_plural = u'Заявки'

admin.site.register(Performer, PerformerAdmin)
admin.site.register(Customer, CustomerAdmin)
admin.site.register(Issue, IssueAdmin)
#admin.site.register(PostBase, PostBaseAdmin)
admin.site.register(Messagekind, MessagekindAdmin)
admin.site.register(Sprkind, SprkindAdmin)
admin.site.register(Sprorg, SprorgAdmin)
admin.site.register(Sitemap, SitemapAdmin)
