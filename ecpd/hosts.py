#from django.conf import settings
from django_hosts import patterns, host

urlpatterns = patterns('ecpd.loc',
    #host(r'www', settings.ROOT_URLCONF, name='www'),
    #host(r'(\w+)', 'ecpd.urls', name='wildcard'),
    host(r'perf', 'ecpd.perf_urls', name='cust'),
    #host(r'cust', 'ecpd.urls', name='perf'),
)
