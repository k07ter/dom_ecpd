#coding: utf-8
from django.conf.urls import patterns, include, url
from django.contrib import admin
from models import *

urlpatterns = patterns('',
    # Examples:

    #url(r'^db_api', include('db_api.urls'), name='db_api'),

    #url(r'^$', 'ecpd.views.home', name='home'),
    #url(r'^lk$', 'ecpd.views.login', name='login'),
    #url(r'^reg$', 'ecpd.views.reg_usr', name='login'),
    #url(r'^lk/(\w+)$', 'ecpd.views.login', name='login'),
    #url(r'^spr/(\w+)$', 'ecpd.views.spr_import', name='spr'),
    #url(r'^cust/', include('customer.urls'), name='cust'),
    #url(r'^perf', include('performer.urls'),name='perf'),
    #url(r'^issue', include('issue.urls'),name='issue'),

    url(r'^$', 'ecpd.views.perf_show', name='perf'),
    url(r'^(\d+)$', 'ecpd.views.perf_one', name='perf'),
    url(r'^add$', 'ecpd.views.perf_new', name='perf'),


    #url(r'^notice/(?P<pk>\d+)/read/$',
    #	login_required(views.ReadNoticeView.as_view()),
    #	name='notice_read'),

    #url(r'^admin/', include(admin.site.urls)),
)
