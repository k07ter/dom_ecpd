"""
Django settings for ecpd project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""
#coding: utf-8

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '(_2&a$)hqhp-5vle-4$)l3$%r4rg!r=&3xk1cpz=ud(g#bl1-f'

from django.conf import settings
from django.utils.cache import patch_vary_headers
#from 
#import models
# import MPSMiddleware
#from django_hosts 
#import django_hosts

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True
TEMPLATE_DIRS = ['templates']
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),
)
ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_hosts',
    'db_api',
    'ecpd',
    #'customer',
    #'performer',
    #'issue',
)

MIDDLEWARE_CLASSES = (
    'ecpd.models.MPSMiddleware',
    #'django_hosts.middleware.HostsMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    #'django_hosts.middleware.HostsMiddleware',
)

#DOMAIN_MIDDLEWARE_MODEL = 'ecpd.Person'
#DOMAIN_MIDDLEWARE_INSTANCE_NAME = 'person'

ROOT_URLCONF = 'ecpd.urls'
ROOT_HOSTCONF = 'ecpd.hosts'

HOST_MIDDLEWARE_URLCONF_MAP = {
    "ecpd.loc:8000": ROOT_URLCONF,
    #"cust.ecpd.loc:8000": "ecpd.urls",
    "perf.ecpd.loc:8000": "ecpd.perf_urls",
}

WSGI_APPLICATION = 'ecpd.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'ecpd', #os.path.join(BASE_DIR, 'db.sqlite3'),
	'USER': 'postgres',
        'PASSWORD': '1'
    },
    'cust': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'customer',
    	'USER': 'postgres',
        'PASSWORD': '1'
    },
    'perf': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'performer',
    	'USER': 'postgres',
        'PASSWORD': '1'
    },
    #'kdc': {
    #    'ENGINE': 'django.db.backends.postgresql_psycopg2',
    #    'NAME': 'kdc',
    #	'USER': 'postgres',
    #    'PASSWORD': '1'
    #}
}
#DATABASE_ROUTER = ['models.DBRouter']

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'ru-ru'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'
SITE_ID = ''
#from settings_mod import *
UPLOADS_ROOT = '/upload/'
