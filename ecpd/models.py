#coding: utf-8

from django.db import models
from django.utils.translation import ugettext_lazy as _
from ecpd import settings

ACCESS_MODE = (
	(0, _(u'Свободный')),
	(1, _(u'Пользователь')),
	(2, _(u'Админ')),
)

BLOCKS = (
	('h', _(u'Заголовок')),
	('tm', _(u'Меню сайта')),
	('tm2', _(u'Под меню')),
	('lm', _(u'Меню услуг')),
	('r', _(u'Реклама')),
	('g', _(u'Галерея')),
)

#from customer.models import Customer
# Create your models here.
from datetime import datetime

class Prox(models.Model):
	class Meta:
		abstract = True

	#def load(self, db, src): pass
	#def dump(self, db, dst): pass

class Person(models.Model):
	class Meta:
		abstract = True

        fam = models.CharField(_(u'Фамилия'), max_length=20)
        name = models.CharField(_(u'Имя'), max_length=15)
	otch = models.CharField(_(u'Отчество'), max_length=30)
	phone = models.CharField(_(u'Телефон'), max_length=10)
	email = models.EmailField(max_length=255, unique=True)
	att_nom = models.CharField(_(u'Номер аттестата'), max_length=10)
	news_signed = models.BooleanField(u'Подписка на новости', default=False)

class Sitemap(models.Model):
	'''Модель сайта'''
	name = models.CharField(_(u'Название'), max_length=30)
	title = models.CharField(_(u'Заголовок'), max_length=30)
	url = models.CharField(_(u'Ссылка'), max_length=30)
	path = models.CharField(_(u'Путь на диске'), max_length=255, blank=True)
	block = models.CharField(_(u'Блок в шаблоне'), choices=BLOCKS, max_length=5, blank=True)
	accessmode = models.IntegerField(_(u'Тип доступа'), choices=ACCESS_MODE, default=0)

	def __unicode__(self):
		return u'%s' % self.name

 	def get_current(self, request):
 	 	meta = request.META
		print(meta)
		return meta

	class Meta:
		db_table = 'sitemap'
		verbose_name = _(u'Страница')
		verbose_name_plural = _(u'Страницы')

class Sprkind(Prox):
	'''Справочник сфер деятельности'''
	kod = models.CharField(_(u'Код'), max_length=10, blank=True, null=True)
#	kind_id = models.ForeignKey(Sprkind, verbose_name=_(u'Сфера деятельности'))
	nazv = models.CharField(_(u'Наименование'), max_length=30)

	def __unicode__(self):
		return u'%s' % self.nazv

	class Meta:
		db_table = 'sprkind'
		verbose_name = _(u'Сфера деятельности')
		verbose_name_plural = _(u'Сферы деятельности')


class Sprorg(Prox):
	'''Справочник организаций'''
	kod = models.IntegerField(_(u'Код'), blank=True, null=True)
	fullname = models.CharField(_(u'Полное наименование'), max_length=255)
	shortname = models.CharField(_(u'Краткое наименование'), max_length=255)
	adress_ur = models.CharField(_(u'Юридический адрес'), max_length=255)
	phone_ur = models.CharField(_(u'Телефон организации:'), max_length=10)
	email_ur = models.EmailField(u'Электронная почта:')
	ogrn = models.CharField(_(u'ОГРН'), max_length=15)
	inn = models.CharField(_(u'ИНН'), max_length=14)
	type_ur = models.ForeignKey(Sprkind, verbose_name=_(u'Сфера деятельности'))
	date_reg = models.DateField(_(u'Дата регистрации на портале'), default=datetime.now)
	#passwd = models.CharField(u'Пароль', max_length=20)
	#passwd = models.ForeignKey(Secure, verbose_name=u'Пароль', max_length=20)
	#service_type = models.ForeignKey(u'Тип услуг', 'Service')	

	class Meta:
		db_table = 'sprorg'
		verbose_name = _(u'Организация')
		verbose_name_plural = _(u'Организации')

	def __unicode__(self):
		return u'%s' % self.shortname

#class Sprspec(Prox):
#	'''Справочник специальностей'''
#	kod = models.CharField(_(u'Код специальности'), max_length=10, blank=True, null=True)
#	kind_id = models.ForeignKey(Sprkind, verbose_name=_(u'Сфера деятельности'))
#	nazv = models.CharField(_(u'Наименование'), max_length=30)

#	def __unicode__(self):
#		return u'%s' % self.nazv

#	class Meta:
#		db_table = 'sprspec'
#		verbose_name = _(u'Специальность')
#		verbose_name_plural = _(u'Специальности')
	
	#def load_data(self, arg, src=None, db=None):
	#	print(arg)
	#	print(self.objects.all())
	#	return 111

class Messagekind(Prox):
	'''Справочник типов cсообщений'''
	#kod = models.IntegerField(_(u'Код'), blank=True, null=True)
	kind = models.CharField(_(u'Тип сообщения'), max_length=30)

	class Meta:
		db_table = 'messagekinds'
		verbose_name = _(u'Тип сообщения')
		verbose_name_plural = _(u'Типы сообщений')

	def __unicode__(self):
		return u'%s' % self.kind

class Message(Prox):
	'''База сообщений (модель)'''
	kat = models.ForeignKey(Messagekind, verbose_name=_(u'Тип сообщения'))
	anons = models.CharField(_(u'Тема'), max_length=80)
	content = models.CharField(_(u'Содержание'), max_length=255)
	#cust_id = models.ForeignKey(Customer)
	#perf_id = models.ForeignKey(Performer)
	sended = models.BooleanField(u'Отправлено', blank=True, default=False)
	response = models.BooleanField(u'Ответ', blank=True, default=False)

	class Meta:
		db_table = 'messages'
		verbose_name = _(u'Сообшение')
		verbose_name_plural = _(u'Сообщения')

#class Subscriber(Prox):
#	'''База подписчиков (модель)'''
#	kat_id = models.ForeignKey(Newskind, verbose_name=_(u'Категория новостей'))
#	client_id = models.CharField(_(u'Категория новостей'), max_length=10)

#	class Meta:
#		abstract = True
		#db_table = 'subscribers'
		#verbose_name = _(u'Подписчик')
		#verbose_name_plural = _(u'Подписчики')

class URLSwitchingMiddleware(object):
    def process_request(self, request):
        request.urlconf = 'ecpd.urls.%s' % request.META['HTTP_HOST'].replace('.', '_')


class MPSManager(models.Manager):
    def get_current(self, request):
 	return request.META

class MPSMiddleware(object):
    """Gets the correct instance of an application-specific model by matching the
    sub-domain of the request."""

    def __init__(self):
        #self.site_domain = Sitemap.objects.get_current().domain
        #if self.site_domain.startswith('www.'):
        #    self.site_domain = self.site_domain[4:]
        #self.SUBDOMAIN_RE = re.compile(r'^(?:www\.)?(?P<slug>[\w-]+)\.%s' % re.escape(self.site_domain))
        try:
             print('dom')
             """
#            app_name, model_name = settings.DOMAIN_MIDDLEWARE_MODEL.split('.', 2)
#            self.model = get_model(app_name, model_name)
#            self.instance_name = settings.DOMAIN_MIDDLEWARE_INSTANCE_NAME
#            assert self.instance_name
	     """
        except (AttributeError, AssertionError):
            raise ImproperlyConfigured('DomainMiddleware requires DOMAIN_MIDDLEWARE_MODEL and DOMAIN_MIDDLEWARE_INSTANCE_NAME settings')

    def process_view(self, request, view_func, view_args, view_kwargs):
        """If domain is not main site, check for subdomain.

        Get the model from the subdomain slug.
        """
        port = request.META.get('SERVER_PORT')
        domain = request.META.get('HTTP_HOST', '').replace(':%s' % port, '')
        """
#        if domain.startswith('www.'):
#            domain = domain[4:]
#        if domain != self.site_domain:
#            match = self.SUBDOMAIN_RE.match(domain)
#            if match:
#                slug = match.group('slug')
#                instance = get_object_or_404(self.model, slug=slug)
#            setattr(request, self.instance_name, instance)
	"""
        return None

    def index(request):
    	print(request.method)
    	"""
#        members = request.club.members.approved().order_by('-creation_date')
#        return list_detail.object_list(
#            request                 = request,
#            queryset                = members,
#            template_name           = 'club/index.html',
#            template_object_name    = 'member'
#        )
	"""

#class Service():
#	name = models.CharField(u'Наименование', max_length=10)

#class DBRouter(object):
#	def db_for_read(self, model, **kwargs):
#		return model._meta.model_name
#
#	def db_for_write(self, model, **kwargs):
#		return model._meta.model_name
#
#	def db_for_delete(self, model, **kwargs):
#		return model._meta.model_name

ISSUE_STAT = (
	(1, u'Одобрено'),
	(0, u'Отказано'),
)

class Performer(Person):
	#email = models.EmailField(u'Эл. ящик')
	#kod = models.CharField(u'Код', max_length=10)
	#phone = models.CharField(u'Телефон', max_length=12)
	#work_kind = models.ForeignKey(u'Тип деятельности', 'Jobkind')
	#news_signed = models.BooleanField(u'Подписка на новости', blank=True, default=False)
	documenty = models.FileField(u'Документы', upload_to=settings.UPLOADS_ROOT+'docs/', blank=True)

	class Meta:
		db_table = 'performers'
		verbose_name = u'Подрядчик'
		verbose_name_plural = u'Подрядчики'

	def __unicode__(self):
		return u'%s, %s' % (self.fam, self.name)

PAYMENT_PERIOD = (
	('W', u'Неделя'),
	('M', u'Месяц'),
)
PAYMENT_STATUS = (
	(1, u'Оплачено'),
	(0, u'Не оплачено'),
)

# Create your models here.
class Customer(Person):
	#custid = models.CharField(u'ID заказчика', max_length=12)
	org = models.ForeignKey(Sprorg, verbose_name=u'Организация')
	fam_ruk = models.CharField(u'Фамилия', max_length=25)
	name_ruk = models.CharField(u'Имя', max_length=15)
	otch_ruk = models.CharField(u'Отчество', max_length=35)
	#fam_usr = models.CharField(u'Фамилия:', max_length=25)
	#name_usr = models.CharField(u'Имя:', max_length=15)
	#otch_usr = models.CharField(u'Отчество:', max_length=35)
	status_usr = models.CharField(u'Должность', max_length=35)
	#phone_usr = models.CharField(u'Телефон основного пользователя:', max_length=10)
	#email_usr = models.EmailField(u'Электронная почта основного пользователя:')
	#other_cont_usr = models.CharField(u'Другие контакты:', max_length=100, blank=True)
	#login = models.CharField(u'Логин:', max_length=20)

	#payment_period_type = models.CharField(u'Период оплаты', choices=PAYMENT_PERIOD, default='w', max_length=3)
	#payment_status = models.IntegerField(u'Статус оплаты', choices=PAYMENT_STATUS, default=0)
	#documenty = models.FileField(u'Юридические документы')

	def show_demo(self):
		cust = [] #Customer.objects.using('cust').all()
		return render_to_response('customer/index.html', {'cust': cust})

	class Meta:
		db_table = 'customers'
		verbose_name = _(u'Заказчик')
		verbose_name_plural = _(u'Заказчики')

	def __unicode__(self):
		return u'%s, %s' % (self.fam, self.name)

# Create your models here.
class Issue(Message):
	issue_nom = models.CharField(u'Номер заявки', max_length=10)
	cust = models.ForeignKey(Customer, verbose_name=u'Заказчик')
        perf = models.ForeignKey(Performer, verbose_name=u'Исполнитель')
	#kurat = models.ForeignKey(Performer, verbose_name=u'Куратор')
	cost = models.IntegerField(u'Решение по МП', choices=ISSUE_STAT, blank=True)
	status = models.IntegerField(u'Решение по МП', choices=ISSUE_STAT, blank=0)
	date = models.DateTimeField(u'Дата решения', default=datetime.now)

	#def list(self, request):
	#	return self.objects

	class Meta:
		db_table = 'issues'
		verbose_name = u'Заявка/Сообщение'
		verbose_name_plural = u'Заявки/Сообщения'

	def __unicode__(self):
		return u'%s' % self.issue_nom
