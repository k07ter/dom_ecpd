#coding: utf-8
from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('db_api.views',
    url(r'$', 'index'),
    url(r'/add/$', 'action'),
)
